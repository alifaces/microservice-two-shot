import { Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

const ShoeList = () => {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8080/api/shoes/')
        if (resp.ok) {
            const data = await resp.json()
            setShoes(data.shoes)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const handleDelete = async (e) => {
        const url = `http://localhost:8080/api/shoes/${e.target.id}`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()

        setShoes(shoes.filter(shoe => String(shoe.id) !== e.target.id))
    }

    return <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>List of Shoes</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Manufacturer</th>
                                <th>Model Name</th>
                                <th>Color</th>
                                <th>Picture</th>
                                <th>Bin #</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                shoes.map(shoe => {
                                    return (
                                        <tr key={shoe.href}>
                                            <td><button onClick={handleDelete} id={shoe.id} className="btn btn-danger">Delete</button></td>
                                            <td>{shoe.manufacturer}</td>
                                            <td>{shoe.model_name}</td>
                                            <td>{shoe.color}</td>
                                            <td>{shoe.picture_url}</td>
                                            <td>{shoe.bin}</td>
                                        </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default ShoeList;
