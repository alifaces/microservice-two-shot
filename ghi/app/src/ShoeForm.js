import React, {useState, useEffect} from 'react';

function ShoeForm() {
    const [bins, setBins] = useState([])
    const [formData, setFormData] = useState({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin: ''
    })


    const getData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json()
            setBins(data.bins);
        }
    }

    useEffect(() => {
        getData();
    }, [])

    const handleSubmit = async (event) => {
            event.preventDefault();

            const binUrl = `http://localhost:8080/${formData.bin}shoes/`;

            const fetchConfig = {
                method: "post",
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

        const response = await fetch(binUrl, fetchConfig)

        if (response.ok) {
            setFormData({
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: ''
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input value={formData.manufacturer} onChange={handleFormChange} placeholder="Name" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="name">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.model_name} onChange={handleFormChange} placeholder="Name" required type="text" name="model_name" id="model_name" className="form-control" />
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.color} onChange={handleFormChange} placeholder="Name" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="name">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.picture_url} onChange={handleFormChange} placeholder="Name" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="name">Picture URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} name="bin" id="bin" className="form-control">
                                <option value="">Choose a bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>
                                        {bin.id}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}



export default ShoeForm;
