import React, {useState, useEffect} from 'react';

function HatForm() {
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        fabric: '',
        style_name: '',
        color: '',
        picture_url: '',
        location: ''
    })


    const getData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json()
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        getData();
    }, [])

    const handleSubmit = async (event) => {
            event.preventDefault();

            const locationUrl = `http://localhost:8090/${formData.location}hats/`;

            const fetchConfig = {
                method: "post",
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

        const response = await fetch(locationUrl, fetchConfig)

        if (response.ok) {
            setFormData({
                fabric: '',
                style_name: '',
                color: '',
                picture_url: '',
                location: ''
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input value={formData.fabric} onChange={handleFormChange} placeholder="Name" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="name">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.style_name} onChange={handleFormChange} placeholder="Name" required type="text" name="style_name" id="style_name" className="form-control" />
                            <label htmlFor="name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.color} onChange={handleFormChange} placeholder="Name" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="name">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.picture_url} onChange={handleFormChange} placeholder="Name" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="name">Picture URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} name="location" id="location" className="form-control">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>
                                        {location.id}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}



export default HatForm;
