# Wardrobify

Team:

* Anton Z - Shoes Microservice
* Alex H - Im doing hats

## Design

## Shoes microservice

The shoes microservice makes use of very basic RESTful API functionality to draw data from the "bins" model via a poller, and use a Bin value object within the Shoes microservice for the creation of new "shoe" objects, which are sorted by unique bin IDs. This is to ensure that every bin has a unique identifier by which particular shoes can be sorted. The shoe models themselves, have basic API calls that allow for a new shoe to be made, displayed, and deleted.

The React front-end abstracts the calling of the API calls to the user. On the front-end, the user has the ability to view a list of all shoes, delete any particular shoe from that list, or use a form to create a new shoe entirely, with the ability to sort that shoe by a bin number of the user's choice. The shoe list also displays the associated bin number of any particular shoe.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
